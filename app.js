const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello, Express!');// this part will be printed on the screen after successfully running the project.
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);// this will be showning at the console means the terminal 
});
